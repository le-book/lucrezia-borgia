#### XVI

Seit dem Sturze der Riarier von Imola und Forli bebten alle Tyrannen im Kirchenstaat vor Cesar, und auch größere Fürsten, wie die Este und Gonzaga, welche entweder gar nicht, oder nur zum Teil Lehnsleute der Kirche waren, bemühten sich um die Freundschaft des Papstes und seines furchtbaren Sohnes. Cesar hatte sich, als Verbündeter Frankreichs, die Dienste jener beiden Fürsten gesichert, und sie hatten ihn seit dem Jahre 1499 in seinen Unternehmungen in der Romagna unterstützt. Er unterhielt einen lebhaften Briefwechsel mit Ercole von Este, den er, ein junger und unreifer Mensch, wie seinesgleichen, wie seinen Bruder und Freund behandelte. Er teilte ihm seine Erfolge mit und empfing dann von ihm in gleich vertraulichem Tone Gratulationen, deren jede eine diplomatische, von der Furcht diktierte Lüge war. Die zwischen Cesar und Ercole gewechselte Korrespondenz bewahrt noch das Archiv Este in Modena; sie ist zahlreich und beginnt mit dem 30. August 1498, wo Cesar noch Kardinal war. Er meldete in diesem lateinisch geschriebenen Brief dem Herzog, daß er nach Frankreich abreise, und bat ihn um ein Reitpferd.

Einen nicht minder vertraulichen Briefwechsel unterhielt Cesar mit Francesco Gonzaga. Er trat zu diesem in ein lebhaftes Verhältnis, welches sogar bis zu seinem Ende fortdauerte. Das Archiv des Hauses Gonzaga in Mantua bewahrt noch einundvierzig Briefe von jenem an den Marchese und dessen Gemahlin Isabella. Der erste datiert vom 31. Oktober 1498 aus Avignon, der zweite vom 12. Januar 1500 aus Forli; der dritte ist dieses Inhalts:

»Erlauchter Herr, wie ein Bruder zu verehren. Wir erfuhren aus den Briefen Ew. Exzellenz die ersehnte und glückliche Geburt Ihres Erlauchten Sohnes mit nicht minderer Freude, als wir bei der Geburt eines eigenen Sohns würden empfunden haben. Da wir aus innigem und brüderlichem Wohlwollen Ihnen jeden Zuwachs und jedes Glück gönnen, so willigen wir gern darein, Gevatter zu sein, und bestimmen als unseren Stellvertreter denjenigen Ihrer Räte, welchen Ew. Exzellenz erwählen wird. An unserer statt möge er das Kind aus der heiligen Taufe heben. Wir bitten Gott unseren Herrn, Ihnen dasselbe nach unserem gemeinschaftlichen Wunsche zu erhalten.

Ew. Herrlichkeit mag es nicht beschwerlich fallen, auch Ihrer Erlauchten Gemahlin in unserem Namen Glück zu wünschen. Dieselbe wird, so hoffen wir, mit diesem Sohn die Reihe zahlreicher Nachkommenschaft begonnen haben, welche den Ruhm so Erlauchter Eltern verewigen soll. Rom, im apostolischen Palast, am 24. Mai 1500. Cesar Borgia von Frankreich, Herzog von Valence, und der heil. röm. Kirche Bannerträger und Generalkapitän.«

Der Sohn des Markgrafen von Mantua war der am 17. Mai 1500 geborene Erbprinz Federigo. Zwei Jahre später, wo Cesar auf dem Gipfel seiner Macht stand, bewarben sich dieselben Gonzaga um die Ehre, diesen ihren Sohn mit seiner kleinen Tochter Luisa zu verloben.

Cesar blieb mehrere Monate in Rom, um Geldmittel für seine Unternehmungen in der Romagna herbeizuschaffen. Ein Zufall hätte alle seine Pläne in einem einzigen Moment zertrümmern können, wenn nämlich sein Vater am 27. Juni 1500 von dem Zusammensturz eines Kamins im Vatikan wäre erschlagen worden. Man zog ihn leicht verwundet aus dem Schutt hervor. Von niemand als von seiner Tochter wollte er sich pflegen lassen. Als der venezianische Botschafter ihn am 3. Juli besuchte, fand er bei ihm Madonna Lucrezia, Sancìa und deren Gemahl Jofré, und ein Hoffräulein Lucrezias, welches die »Favoritin« des Papstes war. Und dieser Papst war siebzig Jahre alt. Seine Rettung schrieb er der Jungfrau Maria zu, wie in unseren Zeiten Pius IX. die seinige aus dem Zusammensturz des Hauses bei S. Agnese derselben Heiligen zugeschrieben hat. Alexander ließ ihr zu Ehren am 5. Juli ein Hochamt halten, und später nach seiner Herstellung sich in Prozession nach S. Maria del Popolo tragen, wo er der himmlischen Jungfrau einen mit dreihundert Dukaten gefüllten Pokal darbrachte. Der Kardinal Piccolomini schüttete dieses Gold mit Ostentation vor allem Volk über dem Altar aus.

Die Heiligen des Himmels hatten sich zwischen die fallenden Mauern im Vatikan und einen großen Sünder gestellt, aber sie ließen ruhig ein Verbrechen geschehen, welches nur achtzehn Tage nach jenem Einsturz an einem Unschuldigen ausgeführt wurde. Den jungen Alfonso von Biselli hatten vergebens eigene Ahnungen und die Ratschläge seiner Freunde ein Jahr zuvor gemahnt, sich dem Verderben durch die Flucht zu entziehen. Er war als ein Opferlamm seiner Gemahlin nach Rom gefolgt, nur um den Dolchen der Meuchelmörder zu erliegen, vor welchen ihn jene nicht retten konnte. Cesar haßte ihn wie das ganze Haus Aragon. Auch war die Ehe seiner Schwester mit einem Prinzen Neapels jetzt so gut bedeutungslos geworden, wie es einst jene mit Sforza von Pesaro gewesen war, vielmehr sie hinderte die Absichten Cesars, welcher eine ihm selbst einträglichere Heirat Lucrezias ins Auge gefaßt hatte. Da nun ihre Ehe mit dem Herzog von Biselli nicht kinderlos geblieben war und folglich nicht geschieden werden konnte, so beschloß er eine radikale Trennung der Ehegatten.

Am 15. Juli (1500) begab sich Alfonso aus seinem Palast nach dem Vatikan, wo sich seine Gemahlin befand. Es war nach elf Uhr nachts. An der Peterstreppe fielen Vermummte mit Dolchen über ihn her. An Kopf, Arm und Schenkel schwer verwundet, vermochte der Prinz in das Gemach des Papstes zu stürzen. Beim Anblick ihres blutenden Gatten sank Lucrezia ohnmächtig zu Boden.

Man trug Alfonso in ein Gemach des Vatikans; ein Kardinal gab ihm die Absolution. Doch seine Jugend siegte, er genas. Lucrezia, welche der Schrecken fieberkrank machte, und seine Schwester Sancìa pflegten ihn; sie kochten ihm selbst die Speisen, der Papst selbst stellte ihm Wächter auf. Man sprach in Rom vielerlei über diesen Frevel und ihren Täter. Am 19. Juli schrieb der venezianische Botschafter an seine Signorie: »Man weiß nicht, wer den Herzog verwundet hat, aber man sagt, es sei dieselbe Person gewesen, welche den Herzog von Gandìa ermordete und in den Tiber warf. Monsignor von Valencia hat ein Edikt erlassen, daß niemand vom Kastell S. Angelo bis nach dem S. Peter sich mit Waffen solle sehen lassen, bei Strafe des Todes.«

Mit teuflischer Ironie sagte Cesar zu demselben Botschafter: »Ich habe den Herzog nicht verwundet, aber wenn ich es getan, so wäre das von ihm wohl verdient gewesen.« Sein Haß gegen seinen Schwager muß auch sehr persönliche Motive gehabt haben, die uns dunkel geblieben sind. Er wagte es sogar, den Kranken zu besuchen; hinausgehend sagte er sodann: »Was nicht am Mittag geschehen ist, das kann am Abend geschehen.«

So vergingen peinvolle Tage, bis der Mörder die Geduld verlor. Am 18. August, um neun Uhr abends, kam er wieder; Lucrezia und Sancìa jagte er aus dem Gemach des Schwagers; er rief seinen Hauptmann Micheletto, und dieser erwürgte ihn. Ohne Sang und Klang, mit gräßlichem Schweigen, als wie in einem Schattenspiel, ward der tote Prinz in den Sankt Peter fortgetragen.

Die Sache war kein Geheimnis mehr. Offen erklärte Cesar, daß er den Herzog umgebracht habe, weil er ihm selbst nach dem Leben trachtete, und er behauptete, daß Alfonso von Bogenschützen nach ihm habe schießen lassen, als er sich im vatikanischen Garten erging.

Nichts offenbart so sehr die furchtbare Gewalt, welche Cesar über seinen lasterhaften Vater erlangt hatte, als diese Tat, und die Weise wie jener, der Papst, sie aufnahm. Aus den Berichten des venezianischen Botschafters geht hervor, daß sie wider den Willen Alexanders geschehen war, daß er den unglücklichen Prinzen sogar zu retten gesucht hatte. Nachdem aber die Tat vollbracht war, ging der Papst über das Verbrechen schnell hinweg, sowohl weil er es nicht wagte, Cesar, welchem er doch den Brudermord vergeben hatte, zur Rechenschaft zu ziehen, als weil ihm selbst die Folgen des Mordes nur erwünscht waren. Er wird es sich erspart haben, seinem Sohn unnütze Vorwürfe zu machen, über deren Sentimentalität, wenn ein Borgia überhaupt solcher fähig sein konnte, Cesar nur würde gelächelt haben. Oder war die Sorgfalt, mit welcher Alexander seinen unglücklichen Schwiegersohn hatte bewachen lassen, nichts als trügerischer Schein gewesen? Wir haben wahrlich keine Gründe, dem Verdacht entgegenzutreten, daß der Papst diesen Mord entweder selbst geplant oder doch ihm zugestimmt hatte.

Nie sank eine Bluttat so schnell in Vergessenheit. Von der Ermordung eines Prinzen des königlichen Hauses Neapel machte man nicht mehr Wesens, als von dem Tode irgendeines vatikanischen Stallknechts. Kein Mensch zog sich deshalb vor Cesar zurück, kein Priester verweigerte ihm den Eintritt in die Kirche, und kein Kardinal hörte auf, ihm mit tiefen Reverenzen zu nahen. Prälaten eilten, den roten Hut von der Hand des allmächtigen Mörders zu empfangen, denn um teures Geld bot er die Kardinalswürde an den Meistbietenden aus. Er brauchte Geld, um seine Eroberungen in der Romagna fortzusetzen. Es waren in diesen Augusttagen bei ihm seine Condottieri Paul Orsini, Julius Orsini, Vitellozzo Vitelli und Hercules Bentivoglio. Siebenhundert Schwergewaffnete hatte sein Vater für ihn ausgerüstet, und am 18. August berichtete der venezianische Botschafter an seine Signorie, daß er vom Papst beauftragt sei, den Dogen zu bitten, er möge von der Protektion der Signoren Riminis und Faenzas Abstand nehmen. Man unterhandelte mit Frankreich um praktische Unterstützung Cesars. Am 24. August zog der französische Gesandte in Rom ein, Louis de Villeneuve; bei S. Spirito ritt eine Maske auf ihn zu und umarmte ihn. Die Maske war Cesar. So offen er seine Frevel sonst trieb, so sehr liebte er es doch, in Rom maskiert einherzugehen.

Der junge Alfonso von Aragon ist die am meisten tragische Gestalt unter den Opfern der Borgia, und sein Schicksal noch ergreifender als das Astorres Manfredi. Wenn Lucrezia, wie man allen Grund zu glauben hat, ihren Gatten wirklich liebte, so mußte sie dessen Ende zur Verzweiflung bringen, und selbst wenn sie keine Leidenschaft für ihn empfunden hatte, mußte sich jedes Gefühl in ihr gegen den Mörder empören, von dessen teuflischer Selbstsucht sie das Opfer war. Und auch gegen ihren Vater mußte sie sich auflehnen, der diese Freveltat so gleichgültig behandelte.

Die lakonischen Berichte aus jenen Tagen schildern uns nicht den Zustand, in dem sie sich gleich nach der Tat befand, noch die Vorgänge, die auf dieselbe im Vatikan unter den Mitgliedern des Hauses Borgia stattfanden. Lucrezia war fieberkrank; aber sie starb weder vor Gram, noch erhob sie sich als Rächerin gegen den Mörder ihres Gemahls, noch floh sie aus diesem schrecklichen Vatikan.

Sie fand sich in der Lage ihrer Schwägerin Donna Maria Enriquez nach dem Tode Gandìas, aber wenn diese damals mit ihrem Sohne in Spanien in Sicherheit war, so gab es für Lucrezia selbst kein Asyl, in welches sie sich ohne den Willen ihres Vaters und Bruders hätte begeben können.

Es würde töricht sein, die Unglückliche zu verdammen, weil sie sich in dem furchtbarsten Augenblick ihres Lebens nicht zur Heldin eines Trauerspiels erhoben hat. In Wahrheit, sie erscheint in ihm sehr schwach und klein. Aber wir haben kein Recht, von Lucrezia Borgia die Leidenschaften einer großen Seele zu verlangen, wenn sie solche nicht besaß. Wir suchen nur sie als das aufzufassen, was sie wirklich war. Und wenn wir richtig urteilen, so war sie eben ein Weib, welches nicht die Macht, sondern nur die Anmut ihrer Natur über das gewöhnliche Maß der Frauen gestellt hat. Dieses junge Weib, das der romantischen Phantasie der Nachwelt wie eine Medea oder wie eine immer lodernde Liebesfackel erschienen ist, hat vielleicht in Wirklichkeit nie eine tiefe Leidenschaft gefühlt. Sie war in der römischen Epoche ihres Lebens stets in Abhängigkeit vom Willen anderer, denn ihr Schicksal wurde erst von ihrem Vater, dann von ihrem Bruder bestimmt. Wie weit, bei tatsächlicher Unfreiheit den Verhältnissen gegenüber, ihr moralischer Widerstand ausreichen konnte, in ihnen die Würde des Weibes zu behaupten, das wissen wir nicht. Wenn aber Lucrezia jemals den Mut besaß, ihre persönlichsten Gefühle und Rechte denen gegenüber geltend zu machen, deren Opfer sie war, so muß sie dies nach der Ermordung ihres Gatten zu tun gewagt haben. Und wohl mag sie damals den mörderischen Bruder mit Anklagen, den Vater mit Tränen bestürmt haben. Die Lästige wollte deshalb Cesar aus der Nähe des Vatikan entfernt wissen, und Alexander schickte sie auf einige Zeit ins Exil, wahrscheinlich weil sie das selbst begehrte. Ein zwischen ihr und dem Vater entstandenes Zerwürfnis deutet der venezianische Botschafter Polo Capello an. Er hatte Rom am 16. September 1500 verlassen, und machte, nach Venedig zurückgekehrt, seiner Regierung über die dortigen Zustände Bericht, wobei er sagte: »Madonna Lucrezia, welche klug und liberal ist, stand zuvor in der Gunst des Papstes, aber jetzt liebt er sie nicht mehr.«

Am 30. August verließ Lucrezia Rom mit einem Gefolge von sechshundert Reitern, um sich nach Nepi zu begeben, von welcher Stadt sie Herrin war. Dort wollte sie sich, wie Burkard sagt, von den Gemütsbewegungen erholen, die ihr der Tod des Herzogs von Biseglia zugezogen hatte.
