_An  
Don Michelangelo Gaetani  
Herzog von Sermoneta_

Mein verehrter Herzog! Ihnen diese Schrift darzubieten, bewogen mich sowohl geschichtliche Verhältnisse, welche in ihr behandelt sind, als eigene persönliche Beziehungen, und beide Gründe haben Sie freundlich anerkannt.

Sie werden Vorfahren Ihres alten und berühmten Hauses in diesem Buche auftreten sehen, aber in nicht glücklicher Gestalt. Die Borgia sind Todfeinde der Gaetani gewesen, und diese entrannen nur mit Not dem Untergang, welchen ihnen Alexander VI. und sein schrecklicher Sohn geschworen hatten. Ihr schönes Sermoneta und alle die großen Güter in der Maritima, welche Ihr Haus seit langen Zeiten besaß, zogen die Borgia ein, und Ihre Vorfahren starben durch sie, oder wanderten ins Exil. Donna Lucrezia wurde Herrin Sermonetas, dann ward ihr Sohn Rodrigo von Aragon mit den Besitzungen der Gaetani als Herzog beliehen.

Jahrhunderte sind darüber hingegangen, und so können Sie jene gewaltsamen Eingriffe in die Rechte Ihres Hauses einer schönen und unglücklichen Dame verzeihen. Auch wurde Ihre Familie bald wieder hergestellt, durch jene Bulle Julius' II., welche Sie als ein Kleinod, auch in bezug auf kalligraphische Ausführung, in Ihrem Familienarchiv bewahren. Seither blieb Ihrem Hause das Erbe ruhmvoller Väter, und Sie selbst sind es, welcher jene alten Besitzungen der Gaetani durch eine musterhafte Verwaltung in neue Blüte gebracht hat.

Das Fortleben historischer Überlieferungen in Dingen wie in Menschen, übt in Rom einen unaussprechlichen Reiz auf jeden Freund der Geschichte aus; auf mich im besondern hat es den mächtigsten Einfluß gehabt, die Fortdauer von Charakteren geschichtlicher Vergangenheit in uralten, noch bestehenden, noch heute blühenden Geschlechtern Roms wahrzunehmen, und mit diesen in persönlicher Beziehung zu sein. Ich habe das Wohlwollen der Colonna, der Orsini und der Gaetani, und alle nur wünschenswerte Förderung durch diese drei berühmten Familien der Stadt erfahren. Sie selbst waren der erste Römer, welcher mir rückhaltlos das Archiv seines Hauses öffnete, dann gab mir die gleiche Gunst der mir unvergeßliche Don Vincenzo Colonna, durch lange Jahre, bis der ehrwürdige Greis im Schloß Marino starb.

Die Gaetani, die Orsini und die Colonna waren längst vom Schauplatz der römischen Geschichte abgetreten. Ihre Familie zumal hatte dies schon seit geraumer Zeit getan. Da kam ein Tag, wo Sie, erlauchter Herzog, Ihr altes Geschlecht in die Geschichte der Stadt Rom wieder zurückführen sollten, in jenem für dasselbe ehrenvollsten Augenblick, als Sie, nach dem Sturze der tausendjährigen Herrschaft des Papsttums über Rom, an die Spitze des städtischen Regiments traten und dem König Victor Emanuel die Ergebenheitserklärung des römischen Volks nach Florenz überbrachten. Dieser denkwürdige Moment, welcher eine lange Epoche der Stadt für immer schloß und eine neue begann, wird in der Geschichte der Gaetani mit Ihrem Namen fortleben und diesen im Erinnern der Römer unvergeßlich machen.

Ich war nicht Zeuge jenes Ereignisses in Rom, aber indem ich davon rede, erinnere ich mich an alles dasjenige, was von öffentlichen und persönlichen Wandlungen, in einer langen Reihe von Jahren, ich in Ihrer Nähe miterlebt habe. Sie und Ihr gastfreies Haus stellten mich in so langer Zeit in die lebendigste Verbindung mit der Geschichte Roms. Von allen meinen Beziehungen zu namhaften Familien Italiens, denen nahezutreten ich die Ehre hatte, ist diejenige zu der Ihrigen die älteste und die persönlichste.

Ich sah Ihre edlen Kinder groß werden, und ich betrachte heute mit Freuden die Schar von jungen Enkeln, welche um Sie her, den Neubegründer der Familie Gaetani, emporzuwachsen beginnt. Mögen sie blühen und Ihr uraltes Geschlecht noch durch lange und glückliche Zeiten fortsetzen, und mögen dasselbe noch in der fernsten Zukunft Taten und Namen edler Männer und Frauen zieren.

Es ist mit diesem Wunsche, daß ich Ihnen diese Schrift überreiche, die mit Ihrem Namen geschmückt ist. Ich weiß, daß Sie dieselbe mit einer Güte aufnehmen werden, welche so groß ist als die Anspruchslosigkeit, mit der ich sie Ihnen darbiete. Denn für mich bedeutet sie ein erwünschtes Zeichen, welches ich im Haus der Gaetani niederlege, ein Zeichen dankbarer Erinnerung, tiefster Verehrung gegen Sie, und der großen Ergebenheit, die mich stets Ihrer erlauchten Familie verpflichten wird.

Rom, am 9. März 1874

_Gregorovius_
