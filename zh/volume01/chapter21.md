#### XXI

卢克蕾齐娅迫不及待地想离开罗马，她告诉费拉拉的使节，罗马对她来说就像一座监狱；公爵也同样迫不及待地想看到这笔交易最终完成。但是，新的册封诏书迟迟没有下达，而且如果没有红衣主教朱利安-罗维雷（Julian Rovere）的同意，就无法割让森托（Cento）和皮耶（Pieve）。因此，埃尔科雷推迟了新娘的随行人员，尽管随着冬天的临近，这个季节越来越不适合进行如此艰苦的旅行。卢克蕾齐娅一见到费拉拉的使节，就会问他们护卫队什么时候来接她。她努力排除障碍。虽然红衣主教们在教皇和塞萨尔面前战战兢兢，但他们还是犹豫不决，不愿意签署让费拉拉的封建利益脱离教会的诏书，更不愿意将这一法令扩大到阿方索和卢克蕾齐娅的所有后代，最多授权到第三代。公爵紧急写信给摩德纳红衣主教和卢克蕾齐娅，她终于在 10 月份成功地使这件事得到了认可，并因此得到了公公的高度赞扬。从十月上半月起，她写给公爵和公爵写给卢克蕾齐娅的几封信被保存了下来。这些信件表明两人之间的关系越来越亲密。显然，埃尔科勒开始接受这桩糟糕的婚姻，因为他发现儿媳比他想象的更加善解人意。她亲自给他写了充满奉承的信，尤其是当她听说公爵身体不适的时候，埃尔科勒感谢她亲手写信给他，他在信中看到了感情的特殊证明。

使节们向他报告说："当我们向尊贵的公爵夫人报告阁下生病的消息时，公主殿下表现出了极大的悲伤；她脸色苍白，站在原地沉思了一会儿。她非常遗憾自己没能在费拉拉亲手照顾阁下，如果您允许的话。当梵蒂冈大厅倒塌时，她照顾了教皇陛下两个星期，在此期间她没有休息，因为教皇只希望得到她双手的治疗"。

公公的病一定吓坏了卢克蕾齐娅，因为他的死即使不会取消她与阿方索的结合，也肯定会推迟。但她还没有证据证明她未来丈夫的不情愿已经消退。在这段时间里，阿方索没有给她写过一封信，卢克蕾齐娅也没有给他写过一封信，这种完全的沉默至少是显而易见的。想到父亲可能会死，卢克蕾齐娅一定会更加难过，因为他的死将不可避免地切断她与阿方索的关系。亚历山大在埃尔科勒生病后不久就病倒了。他感冒了，还掉了一颗牙。为了防止夸大其词的谣言传到费拉拉，他派人找来了公爵的特使，命令他写信给主人，说他的身体不适并不意味着什么。教皇说，如果公爵在这里，虽然我脸上缠着绷带，但我还是会邀请他和我一起去打野猪。特使在信中说，为了他的健康，教皇最好不要在天亮前离开皇宫，而是在天黑时才回来。他说，这是他的坏习惯，是有人爱护他才这样做的。

埃尔科雷和教皇收到了来自各方的祝贺。红衣主教和使节们在信中赞美了卢克蕾齐娅的美貌和智慧。西班牙驻罗马大使对她赞不绝口，埃尔科勒感谢他为儿媳的美德所做的见证。就连法国国王也对这一事件表示出了极大的喜悦，因为他现在意识到，这将给费拉拉国带来最大的利益。在主教会议上，教皇满脸喜悦地宣读了这位君主和他的妻子写给他的贺词。路易十二还特意给圣母卢克蕾齐娅写了一封信，并在信的末尾亲笔写了两个字；亚历山大非常高兴，他把这封信的副本寄到了费拉拉。但马克西米利安的宫廷却没有收到类似的信件。相反，皇帝很不高兴，以至于埃尔科勒变得焦躁不安，正如这封写给他在罗马的两位全权代表的信所显示的那样：

费拉拉公爵等。

"我们最亲爱的 自从米歇尔-雷莫林（Messer Michele Remolines）离开这里后，我们就没有再向我们的教皇陛下汇报过罗马最尊贵的国王对他的看法。但现在有一个可靠的人告诉我们，国王陛下很不高兴，非常责备教皇陛下，而且还没有意识到这个阴谋。他在婚前就给我们写了信，建议我们不要与他结盟，你们可以从这些信的副本中看到这一点。随信附上。这些信件已经给教皇陛下的使节们看过。虽然就我们自己而言，我们并不太在意宗座的这一意见，因为我们是基于理性行事的，而且我们每天都对此感到越来越满意，但出于对我们与宗座的关系的考虑，为了让宗座能够根据她的智慧对这一论证做出判断，我们还是应该将我们的意见传达给她。我们相信，教皇陛下会用她的智慧来审视和认识上述分歧对教皇陛下的重要意义。

因此，如果您认为合适的话，请将所有情况告知她，并让她查看副本；但请以我们的名义要求她不要将副本的作者归咎于我们，即使在我们因紧急原因允许副本落入他人之手的情况下也不要这样做。费拉拉，1501 年 10 月 23 日。

公爵再也无法动摇了。10 月初，他已经选好了新娘的护卫，他承认护卫离开费拉拉取决于他与教皇谈判的进展。费拉拉人和罗马人的婚礼随行人员由哪些人组成是一个非常重要的问题；杰拉尔迪（Gerardi）的一封信函提供了这方面的信息。

"尊敬的阁下等 今天（6 日），我们赫克托和我带着教皇陛下上个月 26 日和本月 1 日的信件以及婚礼随行人员名单，与教皇单独会面。这让教皇陛下非常高兴；在他看来，这份名单既尊贵又丰富，尤其是因为名单中精确地描述了人员的等级和素质。据我所知，教皇阁下的表现超出了教皇的预期。在我们与教皇陛下交谈了一段时间后，教皇召见了罗马涅公爵和红衣主教奥尔西尼家族；埃尔纳主教、特罗切主教和阿德里亚诺先生也出席了会议。教皇希望再次宣读这份名单，这份名单得到了更多的赞扬，尤其是公爵，他说他与名单上的几个人都很熟。他还保留了名单，当我把名单还给他时，他非常感谢我，因为他想把名单还给我。

我们一直在努力争取与公爵夫人一同前来的仪仗队名单，但目前还没有确定。教皇陛下说，他们中很少有女士，因为这些罗马女人在马背上有些狂野和笨拙。目前，公爵夫人的房子里有五六位女士，四位非常年轻的女孩和三位年长的女士，她们将留在公爵夫人身边。也许还会增加一位或另一位。有人告诉她，在费拉拉会有无数的名媛淑女向她投怀送抱，从而巧妙地劝阻了她。和她在一起的还有红衣主教波吉亚的妹妹、嫁给奥尔西尼家族的圣母希罗尼玛。她身边将有三位女士陪伴。到目前为止，他们还没有其他的贵妇人。我相信，正如我所听说的，他们会努力在那不勒斯寻找，但据说只能找到几位，而且只能陪伴公爵夫人。乌尔比诺公爵夫人说，她打算带 50 匹马来。至于男人，教皇陛下说他们也很缺乏，因为除了奥尔西尼家族，罗马已经没有其他贵族绅士了，而且他们大多都在外面。但他希望能找到足够的人数，尤其是如果罗马涅公爵不出征的话，因为教皇陛下的随从中有很多贵族。教皇说，可以派足够多的司铎和有学问的人，但没有合适的人选；不过，教皇的护卫将提供一个和另一个的替代品，更何况，根据教皇的说法，按照惯例，新郎要派大批护卫，而新娘只带几个人前来。不过，我相信她会有不少于两百人骑马随行。教皇对公主殿下的出行路线有疑问；他认为她应该取道博洛尼亚，并说佛罗伦萨人也邀请了她。虽然教皇还没有下定决心，但公爵夫人告诉我们，她将从马尔凯出发，她说教皇已经决定了。也许他希望她从罗马涅公爵的领地前往博洛尼亚。

关于教皇陛下希望红衣主教陪同公爵夫人的问题，教皇陛下回答说，任何红衣主教陪同她从罗马出发似乎都不合适，但他已经写信给驻马尔凯的使节萨莱诺红衣主教，让他前往罗马涅公爵的领地，并在那里等候，之后陪同她前往费拉拉并主持婚礼弥撒。他相信红衣主教会这么做的，除非他生病了。不过，如果情况确实如此，教皇陛下也会另作安排。- - -

当教皇在谈话中得知我们未能觐见最尊贵的公爵时，他对此非常不满，并说教皇陛下就有这样的缺陷，里米尼的使节们在这里待了两个月都没能和他说上一句话；他这是在把白天变成黑夜，把黑夜变成白天。他对这种生活方式深表遗憾，不知道自己是否还能维持现有的生活。另一方面，他又对这位杰出的公爵夫人大加赞赏，因为她很聪明，能从容不迫地接见客人，还知道如何在必要时进行爱抚。他高度赞扬了她，说她统治斯波莱托公国让全世界都感到高兴。他高度赞扬了她，并说即使她必须与教皇谈判，公主殿下也知道如何赢得自己的角色。我相信，教皇陛下说她的好话（我认为这是她应得的），而不是说他的坏话，即使后者已经表明了相反的态度。愿我永远得到永恒的荣耀。罗马，10 月 6 日"。

教皇很少有机会不称赞他女儿的美貌和智慧。他将她与当时意大利最著名的女性曼图亚侯爵夫人和乌尔比诺公爵夫人相提并论。有一天，他还向费拉拉的使节谈起了女儿的年龄，指出她将在四月（1502 年）年满 22 岁，而同时恺撒也将年满 26 岁。

他对新娘的选择感到非常满意，因为新娘是埃斯特家族的王子和费拉拉最杰出的人。他还授权博洛尼亚领主的儿子安尼巴莱-本蒂沃格里奥（Annibale Bentivoglio）加入他们，并笑着告诉费拉拉的使节，如果他的主人想派土耳其人去罗马接新娘，他们会很欢迎的。

佛罗伦萨人害怕凯撒，派使者去找卢克蕾齐娅，请她在去费拉拉的路上走他们国家的路；但教皇规定她必须走罗马涅。按照当时的野蛮专横，火车经过的地方必须为火车提供食物。为了不给罗马涅造成过重的负担，人们决定让费拉拉车队取道托斯卡纳前往罗马；但佛罗伦萨共和国拒绝让车队在其境内的任何地方自由通行；它只想在佛罗伦萨城内招待它，或者赠送它一份礼物。

与此同时，费拉拉正在筹备婚礼庆典。公爵向他的朋友们发出了邀请。他甚至想好了在费拉拉向卢克蕾齐娅介绍她的丈夫时要发表的演说；因为在文艺复兴时期，这样的演说被认为是庆典中最重要的时刻，而那将是一场真正华丽的演说。因此，埃尔科勒指示他在罗马的使节们给他送去关于波吉亚家族的笔记，以便演说家可以加以利用。特使们认真地执行了主人的命令，并给他做出了如下答复：

"最杰出的王子，我们最特别的主人。我们按照阁下的吩咐，不遗余力地调查研究波吉亚这个最显赫的家族的一切事迹；因此，我们到处打听，同样，我们自己的人也在罗马这里忙碌，不仅是学者，还有那些我们认为喜欢这些东西的人。虽然我们终于发现，这个家族在西班牙是非常高贵和古老的，但我们并没有发现它的祖先做过什么出色的事情，因为在那个国家，他们过着非常文明和精致的生活，阁下也知道这是西班牙，尤其是巴伦西亚的习俗。

只有从卡利斯图斯（Calixtus）一直到我们这个时代，才有什么值得称道的事迹，尤其是卡利斯图斯本人的事迹，普拉蒂纳（Platina）已经告诉我们足够多了。但这位教皇所做的一切已广为人知。因此，那些需要发表演讲的人将会发现他们面前有一个广阔的舞台。因此，最尊敬的先生，我们所发现的关于这所房子的信息并没有比您已经知道的更多，而只是关于这所房子里的教皇的人以及对他们发表的顺从演说。然而，教皇们的所作所为却说明了关于他们的一切。如果我们有更多发现，我们会告知阁下，我们向阁下致以崇高的敬意。罗马，1501 年 10 月 18 日"。

当老埃斯特家族的公爵读到这封简短的信函时，他可能会会心一笑，因为他发现这封信函的外交诚意太少了，几乎可以说是一种讽刺。此外，英勇的使节们似乎并没有找到正确的来源，因为如果他们向波吉亚最亲密的廷臣，如波尔卡里家族（Porcari）征求意见，他们就会从他们那里得到一份家谱，其中显示波吉亚是阿拉贡古代国王的后裔，即使不是赫拉克勒斯的后裔。

与此同时，教皇和卢克蕾齐娅的不耐烦与日俱增，因为护送新娘的队伍迟迟没有出发，波吉亚的敌人已经开始嘲笑了。公爵宣称，除非把册封圣谕交给他，否则他不会考虑派人去迎接卢克蕾齐娅圣母。他抱怨罗马兑现承诺的速度太慢。他要求至少在仪仗队抵达罗马时用现金支付由威尼斯、博洛尼亚和其他地方的银行处理的嫁妆，并威胁说如果不全额支付，就把嫁妆送回费拉拉，不带走新娘。由于不可能这么快就交出森托和皮维耶，他要求教皇做出保证，要么为他的儿子希波吕托斯（Hippolytus）提供博洛尼亚主教职位，要么提供押金。他要求为他的私生子唐-朱利奥（Don Giulio）和他的使节吉安卢卡-波齐（Gianluca Pozzi）提供好处，卢克蕾齐娅为他争取到了雷焦（Reggio）主教区，就像她从教皇那里为费拉雷斯使节争取到了罗马的一所房子一样。

另一件重要的事情是卢克蕾齐娅的贵重首饰。如今在罗马，人们依然对珠宝情有独钟，贵族家庭的女主人们从不放过任何一个佩戴钻石闪耀光芒的机会，而这些珠宝通常也是她们的必需品。到了文艺复兴时期，这种热情已经达到了正式狂热的程度。埃尔科勒对儿媳说，她希望把珠宝带在身边，而不是卖掉它们；不过，他将以陪嫁的方式送她丰厚的珠宝，因为他英勇地补充说，她本人就是最珍贵的珠宝，她理应拥有比他本人和他的妻子拥有的更多更漂亮的宝石； 虽然他不像萨瓦公爵那样伟大，但他也有能力送她不亚于他所拥有的珠宝的美丽珠宝。

埃尔科雷和儿媳之间的关系是最友好的，因为卢克蕾齐娅不厌其烦地向教皇提出要求，但教皇本人却对公爵的行为深感不安。他敦促他派人护送卢克蕾齐娅前往罗马，并向他保证，在卢克蕾齐娅抵达费拉拉之前，罗马涅的两座城堡将被移交给他。他说，一旦她到了那里，她就会从他那里得到她想要的一切，因为他对她的爱是如此之深，以至于他甚至想在春天去费拉拉拜访她。他甚至怀疑，新娘的护送迟迟未到是因为皇帝的阴谋。11 月，马克西米利安派他的秘书阿戈斯蒂诺-塞门扎（Agostino Semenza）去找公爵，警告他不要让护送人员前往罗马，公爵答应了埃尔科勒的请求。11月22日，公爵给这位帝国全权代表写了一封信，信中他解释说，他已经立即向罗马的使节们发出了信使；马上就要入冬了，所以卢克蕾齐娅回来的时间不合适；如果教皇同意，他可以推迟，但不会与教皇决裂。让国王陛下考虑一下，如果他这样做，教皇就会成为他的敌人；到那时，他将不得不期待教皇的永久迫害，甚至战争。正是为了避免这些危险，他才同意与教皇陛下共谋。因此，他相信教皇陛下不会让他面临这种危险，而是会一如既往地公正地接受他的道歉。

与此同时，埃尔科勒指示他在罗马的使节将皇帝的威胁告知教皇，并向他解释说他会履行自己的义务，但他必须更加迫切地要求颁布诏书，因为任何进一步的拖延都会带来危险。

亚历山大听后勃然大怒，对使节大加指责，并称公爵本人为 "骗子"。随后，埃尔科莱于 12 月 1 日向皇帝的使者宣布，如果不公开与教皇决裂，他就不能再拖延送亲的时间了。同一天，他写信给罗马的使者，抱怨教皇给他的 "商人 "头衔。但他安慰教皇说，他已将新婚队伍从费拉拉出发的时间定在了 12 月 9 日或 10 日。
