......_安
唐-米开朗基罗-盖塔尼
塞莫内塔公爵_

尊敬的公爵大人 我之所以向您提供这本书，既是出于书中所涉及的历史背景，也是出于我个人的感情，而您对这两个原因都表示了感谢。

您会看到您古老而著名的家族的祖先出现在这本书中，但并不是以幸福的形式。波吉亚家族是盖坦尼家族的死敌，亚历山大六世和他可怕的儿子对他们宣誓的厄运，他们才逃过一劫。他们美丽的塞莫内塔和滨海马萨的所有大庄园都被波吉亚夺走了，他们的祖先或死于波吉亚之手，或流亡海外。唐娜-卢克蕾齐娅成为了塞莫内塔的女主人，然后她的儿子阿拉贡的罗德里戈作为公爵受托管理盖塔尼的产业。

几个世纪过去了，您可以原谅一位美丽而不幸的女士对您家族权利的暴力侵犯。您的家族也很快因朱利叶斯二世的诏书而得到了恢复，您将诏书作为珍宝保存在家族档案中，书法也是如此。从那时起，光荣父辈的遗产就一直留在了您的家族，是您通过模范管理让盖坦尼家族的古老财产焕发出新的光彩。

历史传统在事物和人身上的留存，对罗马的每一位历史之友都有着难以言表的吸引力；尤其是对我来说，在罗马古老的、依然存在的、依然繁盛的家族中，历史人物的留存，以及与他们的个人接触，对我产生了最强烈的影响。我得到了科隆纳、奥尔西尼和盖塔尼家族的青睐，也得到了这三个罗马名门望族的鼓励。他们是第一个毫无保留地向我开放其家族档案的罗马人，然后是令人难忘的科隆纳-文森佐阁下，他多年来一直给予我同样的恩惠，直到这位可敬的老人在马里诺城堡去世。

盖塔尼家族、奥尔西尼家族和科隆纳家族早已退出了罗马历史舞台。尤其是您的家族，已经消失了一段时间。有一天，您，杰出的公爵，将带领您的古老家族重回罗马城的历史舞台，在那最荣耀的时刻，在罗马教皇的千年统治垮台之后，您率领城市军团，向维克多-伊曼纽尔国王递交了罗马人民献身佛罗伦萨的宣言。这个值得纪念的时刻，永远地结束了这座城市漫长的时代，开启了一个新的时代，它将以盖塔尼人的名字载入史册，让罗马人难以忘怀。

我没有亲眼目睹罗马的这一事件，但在谈到这一事件时，我想起了在漫长的岁月里，我在您的邻居那里目睹的所有公共和个人的变化。您和您热情好客的房子让我在如此漫长的岁月里与罗马的历史有了最生动的联系。在我有幸与意大利著名家族建立的所有关系中，与贵家族的关系最为悠久，也最为私人。

我见证了您的贵族子弟的成长，今天，我满怀喜悦地看着在您这位盖塔尼家族的新创始人身边开始成长起来的一群小孙子。愿他们茁壮成长，在漫长而幸福的岁月里延续您古老的血脉，也愿在遥远的未来，高贵的人们的事迹和名字依然熠熠生辉。

正是带着这样的愿望，我把这封镌刻着您的名字的信送给您。我知道，您一定会欣然接受它，就像我朴实无华地把它送给您一样。因为对我来说，这是我在盖塔尼家留下的心仪信物，是对您感激的怀念，是对您最崇高的敬意，是我对您显赫家族永远的忠诚。

罗马，1874 年 3 月 9 日

格雷戈维乌斯
