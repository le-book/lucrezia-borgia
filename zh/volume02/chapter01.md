## 第二本书

### Lucretia Borgia in Ferrara

#### I

带领唐娜-卢克蕾齐娅前往费拉拉的车队每天只走很短的路程，即使是这样，对女人来说也是非常劳累的，尤其是在冬天，因为即使是罗马的乡村也会有崎岖不平的阴雨天。

火车在第七天才到达弗利尼奥。我们在此分享费拉兰使节从那里给他们主人的报告，因为报告生动地描述了旅途的情况和他们到那时为止的经历。

"尊敬的阁下。虽然我们从纳尔尼通过罗马和邮局给阁下写信说，我们将每天不间断地从特尔尼到斯波莱托，再从斯波莱托到这里，但最尊贵的公爵夫人和她的妻子们感到非常疲惫，决定在斯波莱托休息一整天，在这里（福里尼奥）再休息一天。因此，我们明天才会离开这里，在下周二，也就是本月 18 日之前不会到达乌尔比诺。因为明天我们将到达诺塞拉，星期六到达瓜尔多，星期天到达古比奥，星期一到达卡格利，星期二到达乌尔比诺，我们将在那里停留一整天，即星期三；然后将于 20 日继续旅行，到达佩萨罗，就像我们在给阁下的其他信中所写的那样，从一个城镇到另一个城镇。

不过，我们可以肯定，公爵夫人将在上述许多城市休息许多整天，因此，毫无疑问，我们将在本月底或下月初到达费拉拉，或许在第二或第三天。因此，我认为应该从这里向阁下报告此事，让您知道我们的位置和我们认为的位置，以便您能够下达您认为最合适的命令。如果您希望我们将抵达费拉拉的时间推迟到 2 月 2 日或 3 日，我们相信这很容易做到。但是，如果您希望我们在本月最后一天或 2 月 1 日到达，请告诉我们，因为在这种情况下，我们会尽量减少休息日，就像我们迄今为止所做的那样。

我之所以提出上述意见，是因为尊贵的卢克蕾齐娅圣母体质孱弱，不习惯骑马，她的妻子们更是如此，而且我们也意识到，她不希望在抵达费拉拉时因旅途劳顿而伤痕累累。

在荣耀女神所经过的所有地方，她都受到了热情、友好和尊贵的接待，妇女们还向她赠送了礼物，一切似乎都是为了荣耀女神着想。这就是她在这些地方受到的宠爱，她在这些地方也非常有名，因为她曾管理过斯波莱托的公使馆。在这里（福里尼奥），她比罗马以外的其他地方受到了更好的接待，也得到了更多的喜悦。因为不仅这个地方的威权统治者，也就是所谓的市政主席，身披红绸斗篷，一直步行到大门口来迎接她，并陪同她到广场上的住所，而且在大门附近也有一个奖杯在迎接她，奖杯上站着一个手持匕首的罗马卢克蕾齐娅的代表。那个人说了几句诗，大意是：因为她的荣耀，她自己在贞洁、谦虚、谨慎和坚贞方面都超越了她，在这里与她相遇，所以她让开了道路，就位了。

接着，一辆凯旋战车驶来，战车前有一个丘比特，战车上站着帕里斯，他手里拿着金苹果。他说了几句押韵的话，大意是：他曾经根据自己的判断，把苹果赐给了维纳斯，因为只有维纳斯的美貌超过了朱诺和帕拉斯；但现在他撤销了自己的判决，把苹果赐给了她，因为她是征服了这三位女神的女人，因为在她身上凝聚着比这三位女神更伟大的美貌、智慧、财富或权力。

最后，我们在广场上遇到了一艘土耳其武装桨帆船，它在半路上向我们靠近。站在船头的一个土耳其人说了几句押韵的诗，大意是他的伟大国王很清楚卢克蕾齐娅在意大利有多么强大，她多么适合做和平的调解人；因此，他派人向她致意，并恳求她归还他在基督教土地上拥有的一切。我们没有努力去获取这些诗句的文本，因为它们并不完全是彼特拉克的诗句，在我看来，这艘船的介绍也并不十分贴切，甚至很不恰当。

我们不要忘记说，她（卢克蕾齐娅）在弗利尼奥外海四百万码的地方受到了所有执政的巴格里奥尼人的欢迎，他们从佩鲁贾和他们的城堡赶来等候她，邀请她去佩鲁贾。

阁下坚持希望从博洛尼亚走水路前往费拉拉，以避免骑马和走陆路带来的不便，我们已经从纳尔尼向阁下报告过了。

我们的主教皇陛下非常关心她的荣耀，希望每天每时每刻都能知道她的行程进展，她必须在每一个地方给教皇陛下写信。这就证实了阁下已经写过的几封信，即教皇陛下对她的爱胜过其他任何血脉相连的人。

我们将不失时机地每天向阁下报告这次旅行的情况以及旅途中发生的事情。

在斯特雷图拉塔勒的特尔尼和斯波莱托之间，显赫的西吉斯蒙多阁下的一名马夫与公爵夫人随从中的一位高贵的罗马人斯蒂法诺-德-法比发生了激烈的口角，而这并不是因为某些情投意合的人的原因。于是，一个叫皮萨盖拉的人，也是杰出的西吉斯蒙多阁下的仆人之一，骑着马冲了过来，打伤了上述斯蒂法诺的马夫的头。斯蒂法诺生性暴躁，傲慢无礼，这让他很不高兴，以至于他宣布不想再往前走了。在进入斯波莱托城堡时，他与杰出的费兰特阁下和西吉斯蒙多阁下擦肩而过，没有向他们打招呼，也没有理睬他们。但是，因为这件事的性质是偶然的，我们都对此深表遗憾，而且皮萨盖拉和西吉斯蒙多阁下的马夫都已经逃走，所以也就没有什么可做的了，科森扎红衣主教、最尊贵的卢克蕾齐娅圣母和其他人都证明斯特凡诺是错的，他才平静下来，和其他人一起继续前行。我们向阁下致意。1502 年 1 月 13 日，殿下的仆人约翰内斯-卢卡斯（Johannes Lucas）和杰勒德斯-萨拉森斯（Gerardus Saracenus）发自弗利尼奥。

注：据我们目前所知，科森扎最尊贵的红衣主教不得越过乌尔比诺最尊贵的公爵的州界。"

从福里尼奥出发，继续经诺塞拉和瓜尔多前往乌尔比诺公国最令人印象深刻的城镇之一古比奥。在距离古比奥两英里的地方，卢克蕾齐娅迎接了伊丽莎贝塔公爵夫人，并护送她前往古比奥的宫殿。两个女人再也没有分开过，因为后者遵守了她的诺言，陪同卢克蕾齐娅前往费拉拉。

红衣主教波吉亚从古比奥返回罗马，他们乘坐亚历山大送给女儿的舒适轿子，经卡利继续前行。1 月 18 日，当车队驶近乌尔比诺时，圭多巴尔多公爵率领整个宫廷前来迎接。他护送卢克蕾齐娅前往他的府邸，费代里戈（Federigo）的宏伟建筑，卢克蕾齐娅和埃斯特的王子们一起居住在那里，而他本人和公爵夫人则出于礼节腾出了府邸。在乌尔比诺以及他领地的其他地方，贵族吉多巴尔多都竖起了波吉亚和法国国王的盾徽。

蒙特费尔特罗家族对卢克蕾齐娅的婚姻一直持勉强的态度，但现在他们出于对费拉拉的考虑和对教皇的畏惧，对他们的客人表示了敬意。他们是在罗马认识卢克蕾齐娅的，圭多巴尔多作为亚历山大的侍从，曾在罗马与奥尔西尼家族进行过不愉快的战争，他们也是在佩萨罗认识卢克蕾齐娅的。现在，他们可能希望乌尔比诺的安全能在卢克蕾齐娅的影响力和友谊中得到支持。但仅仅过了几个月，吉多巴尔多和他的妻子就被他们客人的兄弟的邪恶背叛所陷害，痛苦地离开了他们的国家。

休息了一天后，卢克蕾齐娅和公爵夫人于 1 月 20 日离开乌尔比诺，在圭多巴尔多的陪同下前往佩萨罗。连接两座城市的道路现在是一条舒适的马车路，穿过美丽的丘陵地带，但当时只能让马匹通过。

卢克蕾齐娅带着难堪的心情走进了这座城市，因为在这里她必须面对她的丈夫斯福尔扎，她已经和他断绝了关系，而斯福尔扎正流亡在曼图亚，为复仇耿耿于怀，他也许会出现在费拉拉，扰乱她的婚宴。佩萨罗现在是她哥哥恺撒的财产。他已下令在他领土上的所有城镇为他的妹妹举行盛大的欢迎仪式。一百名儿童身着他的黄色和红色衣服，手持橄榄枝，在佩萨罗的大门口高喊着：公爵！公爵！公爵！迎接她的到来！公爵 卢克蕾齐娅 卢克蕾齐娅！市政当局护送他们前往宫殿，他们曾经的住所。

高贵的女人们带着喜悦的心情在那里接待了她们曾经的女主人，其中就有卢克蕾齐娅-洛佩兹，她曾经是女主人的侍女，现在是吉安弗朗切斯科-阿尔迪齐的妻子。

卢克蕾齐娅在佩萨罗停留了一天，没有露面。晚上，她允许随行的女士们与佩萨罗的女士们共舞，但她自己并没有参加。正如波齐（Pozzi）向埃科勒公爵报告的那样，"她总是呆在自己的房间里，一是为了洗头，二是她天生喜欢独处和与世隔绝"。但她在佩萨罗的行为更能用忧郁的思想来解释。

罗马涅公爵的所有城市都进行了类似的接待；每到一处，地方长官都会在城门口向卢克蕾齐娅赠送城市的钥匙。以恺撒的名义，她现在由他在切塞纳的省督唐-拉米罗-德奥尔科（Don Ramiro d'Orco）陪同，这只狰狞的猎犬一年后才被他安置在那里。

1 月 25 日，他们经里米尼和切塞纳抵达福尔里。那里的宫殿大厅挂满了珍贵的墙纸，甚至天花板上也贴满了色彩鲜艳的布条。为女士们设立了一个护民官。地方官们赠送了食品、糖果和蜡烛。尽管凯撒的长官们，尤其是拉米罗，在罗马涅维持着严格的秩序，但强盗团伙还是让街道变得不安全。由于担心胆大妄为的强盗詹巴蒂斯塔-卡拉罗（Giambattista Carraro）可能会在迎亲队伍经过切尔维亚（Cervia）的地标时对其进行袭击，一支由一千名步兵和一百五十名骑兵组成的卫队被派了过来，并假装这是一支由民众提供的荣誉护卫队。

在法恩扎，卢克蕾齐娅解释说，整个周五她都要留在伊莫拉洗头，因为只有在狂欢节结束后，她才能再洗一次头。我们已经多次指出，洗头是当时的一种如厕程序，因此，洗头一定与特殊的头发护理技术有关。费兰特特使向他的主人报告了卢克蕾齐娅的意图，称这是一个令人遗憾的障碍，会将麦当娜进入费拉拉的时间推迟到 2 月 2 日。同样，费兰特阁下从伊莫拉写信说，卢克蕾齐娅要求在这里休息一天，以便整理首饰和洗头，她说她已经八天没有洗头了，因此开始头痛。

在从法恩扎前往伊莫拉的途中，火车经过了博洛尼亚城堡，乔瓦尼-本蒂沃格里奥（Giovanni Bentivoglio）在西撒的威胁下，被迫将城堡让给了西撒；人们发现城墙已被夷为平地，城内的沟渠也被填平，城名也改成了西撒里纳（Cesarina）。

在伊莫拉休整一天后，1 月 28 日，骑兵队出发前往博洛尼亚。当他们到达这座伟大城市及其威权统治者的领地边界时，本蒂沃格里奥的所有儿子和他的妻子吉尼维拉带着华丽的随从接待了他们，乔瓦尼本人也在门外迎接了他们两毫。

博洛尼亚的暴君，因为法国的保护才从恺撒手中获救，他不遗余力地向敌人的妹妹表示敬意。他率领数百名骑兵，带着她穿过波吉亚、凯撒、教皇、卢克雷齐亚、法兰西和埃斯特的纹章，就像凯旋一样。在他富丽堂皇的宫殿入口处，卢克蕾齐娅接待了骄傲的吉妮芙拉夫人和许多贵妇人。这位大名鼎鼎的女人，佩萨罗的乔瓦尼-斯福尔扎的姑妈，在灵魂深处一定是多么痛恨这个波吉亚啊！然而，将她和她的整个家族永远赶出博洛尼亚的不是亚历山大和恺撒，而是罗维雷的朱利叶斯二世，仅仅过了四年。

1 月 30 日，博洛尼亚举行了盛大的庆祝活动。晚上，本蒂沃格里家族举行了舞会和宴会。

第二天，他们护送卢克蕾齐娅离开，因为她想继续乘船前往附近的费拉拉，这条运河从博洛尼亚通往波河，后来被雷诺河改道切断。

1 月 31 日傍晚，卢克蕾齐娅抵达了距离费拉拉 20 英里的本蒂沃格里奥城堡，她刚到城堡，丈夫阿方索就突然出现了。她大吃一惊，但很快镇定下来，"非常恭敬和优雅地 "接待了他，他也以英勇的姿态回答了她。这位世袭的费拉拉王子一直对他的妻子保持沉默，因为他的妻子是被迫的。当时的人们既没有狂热的多愁善感，也没有我们现在的多愁善感；但即使如此，令人震惊的是，在卢克蕾齐娅和阿方索的婚姻安排和最终确定期间，他们之间完全没有任何书信往来，卢克蕾齐娅和埃尔科勒之间的许多书信也被保存了下来。不管是出于对父亲的谄媚，还是出于礼貌，抑或是出于好奇，这个粗声粗气、单音节的阿方索现在终于从矜持中走了出来。他是乔装打扮而来的。他逗留了两个小时，然后返回了费拉拉。

这次短暂的会面卸下了卢克蕾齐娅心灵上的沉重负担，这两个小时即使不能让阿方索感受到年轻妻子的魅力，也足以让他失去信心。英勇的福里尼奥市民授予卢克蕾齐娅 "巴黎苹果 "并非毫无道理。费拉拉的一位编年史家在谈到这次会面时说道："所有的人，尤其是新娘和她的朋友们都很高兴，因为国王陛下很想见她，并欣然接受了她，这预示着她会受到很好的接待和更高的待遇。

也许没有人比教皇更高兴了。他的女儿马上就告诉了他，因为她每天都给他写信，告诉他她的行程进展，而他每天也都会收到其他人的来信。他仍然怀疑卢克蕾齐娅是否会受到艾斯特家族的欢迎，这让他很担心。在卢克蕾齐娅离开罗马后，他多次请求红衣主教费拉里告诫公爵善待他的儿媳。他说，他已经为她做了很多，以后还会做得更多。他说，免除法拉利的利息，如果用金钱支付，少于二十万杜卡特是不可能得到的，只有签发诏书的费用，办事员们才能索要五六千杜卡特。法国和西班牙的国王不得不在罗马涅公爵的王国每年给他两万杜卡特的年金，以换取那不勒斯利息的减免，而那不勒斯的利息只有一匹白马。然而，费拉拉却白白得到了一切。

公爵于 1 月 22 日回复了红衣主教的劝告，并向他保证他的儿媳会受到最热烈的欢迎。
