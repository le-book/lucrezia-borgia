#### VI

在最初的激动平息之后，卢克雷齐娅可以为自己的好运祝福了，因为如果她不是阿方索的妻子，而是仍然与波吉亚的命运绑在一起，那她会陷入怎样的悲惨境地呢？她很快就相信自己在费拉拉的地位不会动摇。这既要归功于她自身的优点，也要归功于她作为嫁妆给埃斯特家族带来的持久优势。但在罗马，她看到自己的生命岌岌可危，她的弟弟恺撒病倒了，她的孩子罗德里哥-波吉亚和内皮公爵乔瓦尼也在罗马，而暴怒的奥尔西尼家族正急于为波吉亚家族的亲人报仇雪恨。

她敦促公公帮助西泽，保住他的国家。埃尔科雷发现，与其让罗马涅落入威尼斯人之手，不如保住罗马涅对他更有利。他派潘多尔夫-科伦纽奇奥（Pandolfo Collenuccio）去那里鼓励那些人继续忠于他们的公爵。他向驻罗马的使节表达了自己的喜悦之情，因为西撒即将复国。

除了罗马涅之外，亚历山大之子破碎的帝国立即开始崩溃。被他赶走的暴君们回到了自己的城市。吉多巴尔多和伊丽莎贝塔匆匆从威尼斯赶往乌尔比诺，乌尔比诺欢天喜地地欢迎他们。乔瓦尼-斯福尔扎甚至比他们更早从曼图亚返回佩萨罗。贡萨加侯爵首先告诉了他亚历山大去世的消息，然后又告诉了他恺撒生病的消息，斯福尔扎在信中向他表示了感谢：

"杰出的阁下，尊敬的姐夫。我感谢阁下通过您的信件告诉了我一个好消息，那就是瓦伦蒂诺的病情。我为此感到非常高兴，希望现在就结束我的不幸。我向您保证，当我回到祖国时，我将把自己视为阁下的造物，因为您是我一切的主人，也是我个人的主人。如果您有更多关于瓦伦蒂诺的消息，尤其是关于他的死讯，请务必告诉我，因为这会让我感到特别高兴。无论何时，我都衷心向您致意。曼图亚，1503 年 8 月 25 日。

9 月 3 日，斯福尔扎得以向侯爵报告，他在人们的欢呼声中进入了佩萨罗。他立即让人铸造了一枚勋章来纪念这一喜事。奖章的一面刻着他的半身像，另一面是一个破碎的枷锁，上面写着 patria recepta 字样。现在，他充满了复仇的渴望，对佩萨罗的叛乱分子大肆进行没收物品、监禁和判处死刑。他将许多市民吊死在城堡的窗户上。在费拉拉受到卢克蕾齐娅和公爵保护的科伦纽奇奥也很快落入了他的手中。他用虚伪的承诺将他引诱到佩萨罗，然后根据他之前向塞萨尔-波吉亚提交的请愿书将他投入监狱，他声称现在才得知此事。科伦努乔对他的前君主和朋友并非毫无怨言，但他还是认命了，并于 1504 年 7 月安静地死去。

与此同时，卢克蕾齐娅兴奋地关注着罗马发生的事件。在此期间，她写给凯撒的信和凯撒写给她的信都没有留存下来。我们只有他与费拉拉公爵的往来信件，而费拉拉公爵从未停止过给他写信。9 月 13 日，埃尔科莱祝他早日恢复健康，并告诉他自己已向罗马涅各族人民派出信使，劝告他们效忠于他。

西撒在尼皮收到了这封信。他与法国驻罗马大使达成协议，将自己置于法国的保护之下，于是他按照红衣主教的要求，于 9 月 2 日搬到了内皮。他带走了母亲凡诺扎和弟弟约弗莱，无疑也带走了小女儿路易丝，以及两个孩子罗德里戈和乔瓦尼，后者是内皮公爵。法军仍驻扎在那片土地上，这给了他安全感。他若无其事地给当时总部设在坎帕尼亚诺的贡萨加侯爵写信。贡萨加侯爵甚至送给他猎狗作为礼物。此外，还有一封约弗雷写给贡萨加侯爵的信，日期是 9 月 18 日的尼皮（Nepi）。

恺撒在信中得知，他的保护人和朋友昂布瓦兹并没有像他希望的那样成功当选教皇，而是皮科洛米尼当选了。9 月 22 日，这位年事已高、奄奄一息的红衣主教以庇护三世的身份登上了罗马教廷的宝座。他允许恺撒返回罗马，甚至还对他宠爱有加，但波吉亚于 10 月 3 日刚刚返回，奥尔西尼家族就愤怒地站了起来，要求处死他们的敌人。他带着孩子们逃到圣天使城堡，庇护三世于 10 月 18 日去世。

现在，这些孩子除了西泽和亚历山大赐予他们的两位红衣主教作为监护人外，没有其他保护者了。他们的公国立即分崩离析；教皇去世后，盖塔尼家族立即从曼图亚返回，重新夺回了塞莫内塔和所有曾赐予小罗德里戈的财产。内皮要求得到阿斯卡尼奥-斯福尔扎或使徒司库，卡梅里诺再次被最后的瓦拉诺占据。

罗德里戈是比塞利公爵，因此受到西班牙的保护。为了以防万一，亚历山大六世于 1502 年 5 月 20 日从天主教徒费迪南德和卡斯蒂利亚的伊莎贝拉那里获得了一份文凭，根据这份文凭，西班牙王室将波吉亚家族在那不勒斯的所有财产都赐予了波吉亚，在这份文件中，塞萨尔及其继承人、斯奎莱斯的唐-约弗雷、被谋杀的甘迪亚的儿子唐-胡安、卢克齐雷娅-波吉亚公爵夫人以及她的儿子和继承人罗德里哥-波吉亚都榜上有名。艾斯特家族的档案中还保存着卢克蕾齐娅总理府有关罗德里戈财产管理的文件，以及其他一些与小乔瓦尼有关的文件。因为两个孩子最初是一起长大的；卢克雷齐娅在费拉拉照顾他们；1502 年和 1503 年的家庭开支登记册证明了这一点，其中经常记录给孩子唐-罗德里戈和唐-乔瓦尼穿的衣服用的是什么天鹅绒、丝绸或黄金锦缎。

尽管有西班牙的保护，卢克蕾齐娅的儿子当时在罗马仍有生命危险，对她来说，没有什么比夺回她的孩子并把他带回去更重要的了。她没有这样做，是因为她没有被允许这样做，或者是因为她没有足够的胸怀去执行，也可能是因为她担心孩子在费拉拉会有生命危险。罗德里戈的监护人科森扎红衣主教建议她卖掉儿子的所有财产，带他到意大利以外的安全地方，即西班牙。她将此事告知了公公，公公的答复如下

"尊敬的夫人，我们最亲爱的儿媳和女儿。我们收到了老爷的来信，以及您寄给我们的科森扎红衣主教给您的信，我们把它和我们的这封信一起还给您；除了我们自己，没有任何人读过这封信。我们注意到了阁下本人和上述红衣主教在信中所表达的理解，他们的建议非常有见地，只能说是出于爱和智慧的启发。既然我们已经考虑到了一切，那么我们认为，我们的主教大人可以而且可能会同意这位尊敬的阁下的建议。在我看来，为了证明他对您和您最杰出的儿子唐-罗德里科的衷心爱意，我们的大人完全有义务这样做。即使罗德里科阁下离永恒的荣耀还有些距离，但他远而安全总比他近而危险要好，就像红衣主教所暗示的那样。但当他长大成人后，他可以根据具体情况决定是返回意大利还是留在国外。同为红衣主教，为了维持生计和增加收入，有必要将动产兑换成货币，这是他的明智之举，正如他所宣称的那样。总之，正如我们所说，在我看来，同意他的意愿是合适的。不过，如果我们充满智慧的主人认为情况并非如此，我们将把决定权留给你们。永别了 科德戈里奥，1503 年 10 月 4 日，赫拉克勒斯，费拉拉公爵等"。

与此同时，1503 年 11 月 1 日，罗韦尔以朱利叶斯二世的身份登上教皇宝座。 罗韦尔、波吉亚和美第奇王朝，每个王朝都有两位教皇，使教皇制度具有了现代的政治形式。在教会的历史上，没有任何一个家族对历史产生过类似的影响。他们的名字包含了政治和道德革命的伟大背景。现在，罗维雷家族再次取代了波吉亚家族，而他们最凶恶的敌人曾经是朱利安。恺撒的倒台可以说是决定性的。

其他历史记载了朱利叶斯二世是如何先利用凯撒对西班牙红衣主教的影响力来确保自己的当选，然后在罗马涅要塞被攻陷后又将他置于一旁的。1504 年 4 月，西撒投入西班牙的怀抱，从奥斯提亚前往那不勒斯，当时伟大的康萨尔沃督军是天主教斐迪南的省督。唐-约弗雷（Don Jofré）与他同行，索伦托的红衣主教弗朗切斯科-罗莫利尼（Francesco Romolini）和卢多维科-波吉亚（Ludovico Borgia）作为逃避即将到来的审判的逃犯，也先他一步来到了那不勒斯。在这里，康萨尔沃撕毁了他给塞萨尔的担保书。他于 5 月 27 日以斐迪南国王的名义逮捕了他，并将他暂时送往伊斯基亚岛城堡。

我们没有听说波吉亚子女的命运；他们很可能留在了罗马，或者说留在了那不勒斯，受到西班牙红衣主教的保护。恺撒只剩下一条命，他踏上了前往西班牙的船。他已将自己的贵重物品托付给罗马的朋友照看，以便帮他取回或安全运到费拉拉。因此，1503 年 12 月 31 日，埃科尔公爵写信给他在罗马的使节，让他在索伦托红衣主教送来箱子后立即接收，然后作为埃斯特红衣主教的财产送往费拉拉。但在 1507 年 5 月，红衣主教罗莫里尼去世后，朱利叶斯二世没收了恺撒家中的 12 个箱子和 84 包装有墙纸、布匹和其他物品的物品。他的另一部分珍宝、金银和类似的贵重物品被教皇从佛罗伦萨收回，恺撒将它们存放在佛罗伦萨，但佛罗伦萨威权统治者宣称自己不会受到伤害。

西撒返回西班牙引起了轩然大波。无论是康萨尔沃、教皇还是斐迪南国王，没有人愿意安排此事。还有传言说，是甘迪亚的遗孀在西班牙宫廷逮捕了杀害她丈夫的凶手。西班牙的红衣主教们支持恺撒，卢克蕾齐娅也试图让她的弟弟获释。关于他的消息从西班牙传来；第一封是 1504 年 10 月的，科斯塔比里在写给费拉拉的信中说："瓦朗斯公爵的事情似乎并不像人们说的那样绝望，因为萨莱诺的红衣主教收到了公爵大人事先派去的公爵的副官 Requesence 三世的来信。Requesenz在信中说，公爵被安置在塞维利亚的城堡里，虽然城堡很坚固，但很宽敞，只有一个仆人；但后来又给他安排了八个仆人。他还写道，他曾向国王谈起过释放的事，国王回答说，他没有下令监禁公爵，而是为了康萨尔沃指控他的许多事情才下令把他关进城堡的；如果这些事情没有被证明是真的，他就会在西泽尔的问题上偏袒红衣主教。但首先他们必须等待王后康复。他给纳瓦拉国王和王后的使节也是同样的答复，他们为释放公爵进行了最认真的游说，因此 Requesenz 希望他能很快获得自由。

从 Requesenz 的信中可以看出，西撒首先被带到了塞维利亚。从那里他被带到卡斯蒂利亚的梅迪纳德尔坎波城堡。他向法国国王提出的请求无人理睬。在意大利，没有人希望他获得自由。在那里，没有人比他的姐姐更关心这个没落的新贵，而她的努力很难得到埃斯特家族的热心支持。因为如果恺撒再次出现在意大利，他只会使费拉拉宫廷动荡不安，甚至可能使其成为他阴谋活动的中心。只有贡萨加家族似乎并没有完全放弃对凯撒的支持，尽管他们自己并没有如愿以偿地与凯撒结为亲戚，而是成为了罗韦雷家族的亲戚。1505 年 4 月 9 日，曼图亚侯爵将自己的小女儿莱奥诺拉嫁给了尤利乌斯二世的侄子、乌尔比诺的继承人弗朗切斯科-玛丽亚-罗韦雷尔。伊莎贝拉-贡萨噶出于对嫂子卢克蕾齐娅的爱护，特别支持了丈夫的事业。在贡萨加家族的档案中，至今还保存着几封卢克蕾齐娅写给侯爵的支持其弟弟的信。

1505 年 8 月 18 日，她从雷焦写信给他说，她已经开始在罗马进行谈判，希望教皇允许红衣主教彼得罗-伊苏阿莱斯前往西班牙宫廷，以争取西泽尔的释放。因此，她请求侯爵恳求教皇允许红衣主教执行这一任务。11 月 8 日，她再次从贝里瓜尔多写信给他，感谢他派遣一名代理人前往西班牙，同时她还给他寄了一封给斐迪南国王的信和一封给她弟弟西撒的信。

我们不知道这位红衣主教是否真的去了马德里宫廷，也很难相信朱利叶斯二世允许他这么做。
