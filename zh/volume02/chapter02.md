#### II

2 月 1 日，卢克蕾齐娅继续沿着运河前往费拉拉。在马拉贝尔戈（Malalbergo），她找到了专程前来迎接她的伊莎贝拉-贡萨加（Isabella Gonzaga）。这位侯爵夫人是应她父亲的紧急邀请前来参加他宫殿里的盛宴的，她只是勉强响应了这一号召。正如她在给留在家中的丈夫的信中所说，她 "兴高采烈 "地迎接并拥抱了即将到来的嫂子。随后，她陪同嫂子乘船前往托雷斯公国（Torre della Fossa），运河在此汇入波河的一条支流。波河在距离费拉拉四英里的地方蜿蜒流淌，只有一条名为 "费拉拉河"（Po di Ferrara）的支流，即今天的 "森托运河"（Canale di Cento）到达费拉拉城，在那里分为沃拉诺河（Volano）和普里马罗河（Primaro），两条支流都流入亚得里亚海。这两条运河都很狭窄，在运河上旅行既不是一种享受，也不会给人留下深刻印象。

在托雷斯-德拉-托雷家族，公国带着唐-阿方索和他的宫廷在等着他们。当卢克蕾齐娅走下船时，公爵亲吻了她，而她也恭恭敬敬地吻了公爵的手。然后，他们都登上了装饰华丽的布金托罗船，外国使节和许多骑兵在船上向新娘行礼，他们抚摸着新娘的手。在音乐和炮声的伴奏下，他们来到了博尔戈-圣卢卡（Borgo S. Luca），并在那里登陆。在这里，卢克蕾齐娅搬进了埃斯特的阿尔贝托的宫殿，阿尔贝托是埃尔科莱的亲兄弟。在那里，埃斯科莱的亲生女儿卢克蕾齐娅-本蒂沃格里奥（Lucrezia Bentivoglio）和许多名媛贵妇接待了她。公爵的宫廷总管向她介绍了圣母特奥多拉和 12 位夫人，她们将成为她的侍女。她的岳父向她赠送了五辆漂亮的马车，每辆车都有一辆四轮马车。卢克蕾齐娅在那栋别墅里过了一夜。这座乡间别墅已经消失，圣卢卡郊区依然存在，但整个地方已经面目全非。

埃斯特公爵府邸已经挤满了成千上万的来访者，他们都是应公爵的邀请或好奇心驱使而来的。国家的所有骑士附庸都来了，但统治者们却没有。乌尔比诺和曼图亚的统治者由他们的妻子代表，本蒂沃格里家族由安尼巴莱代表；罗马、威尼斯、佛罗伦萨、卢卡、锡耶纳和法国国王都派出了使节，他们在贵族的宫殿里受到了接待。凯撒由他的骑兵代表，但他本人仍留在罗马。另一方面，应亚历山大的要求，他的妻子夏洛特-阿尔布雷特（Charlotte d'Albret）本应从法国来到费拉拉参加婚礼庆典，并在那里逗留一个月，但她没有出现。

埃尔科勒为婚礼做了大量的准备工作，在宫廷和城市的储藏室里储备了数周的食物。文艺复兴也在费拉拉创造了一个知识型的宫廷和一个富裕的市民社会，在那里，研究、艺术和手工艺蓬勃发展，这一点在这次庆典上得到了充分体现。

因此，卢克蕾齐娅在 2 月 2 日的入宫仪式是当时最辉煌的场面之一，对她来说也是她一生的庆典，她在这一天拥有了她的天性所能达到的最高和最好的境界。

下午两点，公爵带着所有使节和宫廷人员前往阿尔贝托的乡间别墅，与儿媳会合。车队列队穿过波河上的大桥，穿过泰达尔多要塞的大门，这座要塞已经不复存在了。

七十五名骑马的弓箭手为队伍开道，他们身着埃斯特家族的白色和红色服装，身后跟着八十名小号手和许多吹笛手。费拉拉的贵族们不分先后地紧随其后；然后是曼图亚侯爵夫人（她本人留在了住宅宫殿）和乌尔比诺公爵夫人的宫廷。在他们身后，唐-阿方索与姐夫汉尼拔-本蒂沃格里奥（Hannibal Bentivoglio）并驾齐驱，身边有八名侍从。他身着法国流行的红色天鹅绒服装，头戴黑色天鹅绒贝雷帽，贝雷帽上有一个打金的装饰品。他穿着黑色天鹅绒的法式紧身裤和一双颜色鲜艳的踝靴。他的棕色骏马全身缀满深红色和金色。

令人吃惊的是，唐-阿方索没有紧随妻子进入费拉拉，但当时的礼仪与我们不同。新郎走在队伍的前面，新娘是队伍的中心，而岳父则走在队伍的最后，这样卢克蕾齐娅就成了庆典的主角。紧随阿方索之后的是她的车队：首先是侍从和宫廷官员，包括几位西班牙骑兵；然后是五位主教；在他们之后是按升序排列的使节，最后是四位罗马代表，他们骑着漂亮的骏马，身披长长的锦缎斗篷，头戴黑色天鹅绒贝雷帽。他们身后跟着六名打手鼓的人和两名卢克蕾齐娅最喜欢的小丑。

然后，新娘骑着一匹身披大红衣裳的白马，带着美丽和幸福的光彩走了过来，新郎们走在她的身旁。卢克蕾齐娅身着黑色天鹅绒宽袖坎莫拉裙，裙边镶有精致的金边，外披金色锦缎镶朱砂的纱巾。伊莎贝拉-贡萨噶叹息着说，她的脖子上戴着一条大珍珠和红宝石项链，这条项链曾经属于费拉拉公爵夫人。她美丽的秀发披散在肩上。她骑在紫色的华盖下，费拉拉的医生们（即法学院、医学院和数学学院的成员）依次佩戴着华盖。

为了向费拉拉和波吉亚的守护神--法国国王表示敬意，卢克雷齐娅召来了他的特使菲利普-德拉-罗卡-贝尔蒂（Philipp della Rocca Berti），将他带到自己的左侧，让他与自己并驾齐驱，但不在顶篷之内。这样的殊荣是为了显示，带领这位新娘进入埃斯特宫的君主是多么强大。

卢克蕾齐娅身后是身着黑色天鹅绒的公爵，他骑着一匹同样覆盖着天鹅绒的黑马。他骑在身穿黑色天鹅绒长袍的乌尔比诺公爵夫人左侧。

紧随其后的是贵族和侍从，然后是埃斯特家族的其他王子，他们每人都骑在卢克蕾齐娅的一位夫人身边。只有红衣主教希波吕托斯因留在罗马而缺席。在卢克雷齐娅的女伴中，只有三人骑在马背上：法比奥-奥尔西尼（Fabio Orsini）的妻子希罗尼玛-波吉亚（Hieronyma Borgia）、另一位奥尔西尼（Orsini），但没有具体说明是谁，以及圣母阿德里亚娜（Madonna Adriana），"一位寡妇贵妇和教皇的亲戚"。

他们身后跟着 14 辆华丽的马车，车上坐着来自费拉拉的装饰精美的贵妇人，其中 12 人将进入年轻公爵夫人的宫廷。随后，两匹白色的骡子和两匹白色的马被牵着走了过来，它们身上覆盖着天鹅绒、丝绸和珍贵的金饰。紧随其后的是由八十六匹骡子组成的队伍，驮着新娘的衣柜和珍宝。当这支长长的队伍在目瞪口呆的人群中行进时，善良的费拉雷人可能会说，阿方索选中的是一位富有的新娘，但很少有人想到，这些拖着华丽装饰的包袱、箱子和箱子，都是基督教烧毁的土地上的废弃财产。

在泰达尔多堡的大门口，卢克蕾齐娅的马被一发炮弹吓了一跳，甩下了场面的主角。新娘站了起来，公爵把她放在另一匹马上，然后队伍向前走去。在凯旋门和看台前，人们照例致以问候，发表宣言，并上演神话场景，其中最引人注目的是仙女们围绕着她们的女王，女王坐在一头红色的公牛上，萨梯尔则在她身边跳跃。桑纳扎尔可能认为，这种对波吉亚纹章的美化是根据他自己的诗句创作的，他曾在诗句中嘲笑朱丽叶-法尔内塞是公牛上的欧罗巴。

当游行队伍到达大教堂广场时，两名走钢丝的人从两座塔楼上走下来，向新娘致意。在那个年代，闹剧总是与喜庆融为一体。

当车队到达大教堂广场和公爵府邸时，夜幕已经降临，此时所有囚犯都获得了自由。所有的小号手和吹笛手都聚集在一处，吹响了他们的乐器。

很难说清楚卢克蕾齐娅进入的府邸位于何处。埃斯特家族在城中建造了许多宫殿，他们轮流居住在这些宫殿中：Schifanoja、Diamanti、Paradiso、Belvedere、Belfiore 和 Kastell Vecchio。1494 年，一位城市编年史编纂者在 "埃斯特家族领主拥有的 "住宅中列出了公爵的科蒂利宫（Palazzo del Cortile）和韦奇奥城堡（Castello Vecchio）、阿方索的韦奇奥城堡（Castello Vecchio）以及红衣主教希波吕托斯的塞托萨宫（Palace of the Certosa）。因此，在 1502 年，埃尔科勒居住在上述两座宫殿中的一座，而且这两座宫殿是相连的，因为一系列建筑从旧城堡一直延伸到大教堂广场，最后才落在德拉里奥宫。尽管所有这些建筑都已改变，但这种联系至今依然存在。

当时公爵的住所就在大教堂对面，有一个带大理石楼梯的大庭院，因此被称为科蒂莱宫（Palazzo del Cortile）。这个庭院可能被保留到了现在的 Cortil Ducale 中。您可以从大教堂广场穿过高高的拱门进入庭院，拱门两端的两根柱子曾经支撑着尼科洛三世和博尔索的雕像。关于卢克蕾齐娅进入公爵府的记载明确指出，她是在大理石庭院的台阶上下马的。

在这里，她受到了贡萨加侯爵夫人和许多贵妇人的款待。阿方索年轻的妻子，如果当时还来得及激动一下的话，可能会微笑着意识到，埃斯特的贵族家族已经准备了一帮相当英俊的私生女来欢迎她，因为她们在楼梯口迎接她： 卢克蕾齐娅是埃尔科勒的亲生女儿，也是安尼巴莱-本蒂沃格里奥的妻子，还有埃斯特的西吉斯蒙德的三个亲生女儿：卡拉拉伯爵夫人卢克蕾齐娅、乌古佐尼伯爵夫人美丽的戴安娜和比安卡-桑塞韦里诺。

夜幕降临了，灯光和火把照亮了宫殿。在音乐声中，这对年轻夫妇被带入接待大厅，在宝座上坐下。宫廷工作人员举行了隆重的仪式，一位演说家可能正在向麦当娜发表庄严的演说，公爵已经为此收集了关于波吉亚家族的笔记。我们不知道这位喜悦的演说家是谁，但我们知道费拉拉的一些诗人曾向美丽的公主献上他们的婚礼卡米娜。尼古劳斯-马里乌斯-帕尼西亚图斯（Nicolaus Marius Paniciatus）为卢克雷齐亚、阿方索和埃尔科莱写下了一系列热情洋溢的拉丁诗歌和寓言，他将这些诗歌和寓言汇编成《博尔基亚家族》一书。其中包括对这对年轻夫妇结婚的热烈祝贺，卢克蕾齐娅的美貌被置于海伦娜之上，因为她的美貌与无与伦比的谦逊相结合。

但在卢克蕾齐娅到来的前一天，印刷商劳伦提斯完成了一首由一位年轻拉丁文作家创作的婚礼诗。这首诗的作者是西利奥-卡尔卡尼尼，他后来成为著名的数学家，是红衣主教希波吕托斯的宠儿，也是伟大的伊拉斯谟的朋友。这首诗的构思非常简单：维纳斯离开罗马，陪伴着卢克蕾齐娅；缪斯女神姆涅摩西涅劝说她的女儿们歌颂这位高贵的公主，她们兴高采烈地照办了。王公贵族们也没有被遗忘，欧特佩（Euterpe）歌颂埃尔科勒（Ercole），特普西科尔（Terpsichore）赞美阿方索，卡利俄普（Calliope）颂扬凯撒在罗马涅的胜利。

在向费拉拉表示敬意的诗人中，还有一位诗人在这一场合出现，甚至在当时就引起了人们对他天才的高度期待。他就是阿里奥斯托，当时年仅 27 岁，在费拉拉的宫廷和意大利的教育界已是著名的拉丁语作家和喜剧诗人。他还写了一首表白诗，并将其献给了卢克蕾齐娅。这首诗轻快优美，没有过多的神话迂腐，但也没有发明创造。诗人赞美了费拉拉城的幸福，因为拥有一颗无与伦比的明珠，费拉拉城从此将成为所有外国人羡慕的地方，而罗马却因为卢克蕾齐娅的离去而变得贫穷，就像再次陷入废墟一样。他将年轻的公主美化为_pulcherrima virg_，甚至在这里暗指古代的卢克蕾齐娅。

招待会结束后，公爵护送儿媳前往为她准备的房间。她对在艾斯特家族受到的接待非常满意，她的个性也给人留下了最美好的印象。编年史家贝尔纳迪诺-赞博托（Bernardino Zambotto）写道："新娘 24 岁（这是他的错觉），面容姣好，眼睛活泼开朗，身材修长，头脑灵活，非常聪明伶俐，性格开朗，举止优雅，富有人情味。她让这些人非常满意，他们都感到非常满意，并期待着她的荣耀能给他们带来保护和良好的统治。他们真的很高兴，因为他们希望通过她，特别是通过教皇的力量，这座城市会得到很多好处，教皇非常爱他的女儿，他给她的嫁妆和给唐-阿方索的城市都表明了这一点"。

卢克蕾齐娅的优雅在当时一定是令人着迷的；她的奖章告诉了我们这一点，目击者也都这么说。帕尔马的卡尼奥洛曾这样描写她："她中等身高，身材玲珑有致；脸庞修长，鼻子轮廓优美，头发金黄，眼睛呈蓝色；嘴巴有点大，牙齿洁白耀眼；脖子纤细洁白，丰满而不失度量。她的整个人总是透着欢快的笑意"。

卡尼奥洛用来形容卢克蕾齐娅眼睛颜色的词是_bianco，在意大利语中仍然是 "蓝色 "的意思。在蒂格里收集的托斯卡纳民歌中，多次提到 _occhi bianchi_，意思是 "蓝色的眼睛"。佛罗伦萨人 Firenzuola 在他的论文《论女人的完美之美》中要求头发是金色的，眼睛是白色的，瞳孔不完全是黑色的，尽管希腊人和意大利人都喜欢这样。他说，眼睛最好的颜色是褐黄色。不确定的蓝色眼睛可能适合卢克蕾齐娅优雅的气质、开朗的面容和金色的头发。费拉拉的诗人们很快就歌颂了他们美丽的公爵夫人那双耀眼的眼睛，但他们对眼睛的颜色却只字未提。

这位奇特的女子令所有人着迷的，不是威严，也不是古典美，而是难以形容的优雅，再加上一些神秘而奇特的东西。雍容华贵的外表，开朗和蔼的谈吐，这些都是露克蕾齐娅的特质，同时代的人都对她赞不绝口。如果你想象一下这张色彩精致、充满灵气的脸庞，一双蓝色的大眼睛，金色的头发，你就会看到一个浪漫的美人，就像莎士比亚想象中的伊莫金一样。
